### Work Cycle Saver

## Checkpoint 1

This is currently a bare bones application demo - a project for me to learn go with, and general application dev form the ground up.

The first checkpoint just provides a couple of example endpoints with end to end logic, although none of them can be considered complete. 

**DISCLAIMER**: I don't really know what I'm doing! I wouldn't use any of the code in here (not yet anyway).

# Project structure
The project is split into 3 packages, mostly for code organisation:
    **models**: Fairly self explanitory package. Whilst it's slightly overkill given that go doesn't have objects, however if I decide to incorporate an ORM or some sort of database utility later it will be useful to have it sperated out. This is why some of the database setup/connection logic lives here as well.
    **app**: Contains the business logic of the application, at the moment basically just responsible for putting/getting stuff from the database.
    **api**: Handles the 'public' interface of the application, including the routing and DTOs, as well as currently some input validation.

# Todos:
* Add actual security, with hashed and salted passwords
* Fix error handling in API to not use panic (should always be returning a response of some sort)
* Investigate if there's a better way to pass around the environment
* Add sql transactions around endpoint logic
* Create some error handling logic  in application that the API can work with
* Consider DTO's having pointer fields and maybe custom deserialisation
* Look into an ORM, otherwise consider using sqlx or perhaps prepared queries
* Add tests
* Add documentation
* Change route registration to read in from file
* Add access/roles control
* Add index logic to return list of endpoints
* Add utility functions for Request/Response handling
* Consider rest api design (how will put work etc)
* Add logging
* Wraps sql logic into some utility functions in application layer
