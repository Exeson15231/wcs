--name:drop-table-email
DROP TABLE IF EXISTS email;

-- name:create-table-email
CREATE TABLE email(
    id BIGSERIAL PRIMARY KEY,
    address VARCHAR(255) UNIQUE NOT NULL,
    subscribed BOOLEAN NOT NULL,
    fk_users_id BIGINT REFERENCES users(id)
);

--name:drop-table-security
DROP TABLE IF EXISTS security;

--name:create-table-security
CREATE TABLE security(
    id BIGSERIAL PRIMARY KEY,
    plain_text_password VARCHAR(255) NOT NULL,
    fk_users_id BIGINT REFERENCES users(id)
);

--name:drop-table-users
DROP TABLE IF EXISTS users;

--name:create-table-users
CREATE TABLE users(
    id BIGSERIAL PRIMARY KEY,
    business_id BIGSERIAL,
    username VARCHAR(255) UNIQUE NOT NULL
);
