DROP TABLE IF EXISTS email;

CREATE TABLE email(
    id BIGSERIAL PRIMARY KEY,
    address VARCHAR(255) UNIQUE NOT NULL,
    subscribed BOOLEAN NOT NULL,
    fk_users_id BIGINT REFERENCES users(id)
);
