DROP TABLE IF EXISTS security;

CREATE TABLE security(
    id BIGSERIAL PRIMARY KEY,
    plain_text_password VARCHAR(255) NOT NULL,
    fk_users_id BIGINT REFERENCES users(id)
);
