package main

import (
	"net/http"
	"wcs/api"
	"wcs/app"
	"wcs/models"
)

func main() {
	database, err := models.Connect("localhost",
		"5432",
		"fd15231",
		"wcs",
		"password")
	defer database.Close()
	if err != nil {
		panic(err)
	}

	env := app.Env{
		Db: database,
	}

	dropExec := []string{
		"drop-table-email",
		"drop-table-security",
		"drop-table-users",
	}

	createExec := []string{
		"create-table-users",
		"create-table-email",
		"create-table-security",
	}

	models.CreateTables("./resources/sql/create_tables.sql",
		env.Db,
		dropExec,
		createExec)
	router := api.GenerateRouter(&env)
	http.ListenAndServe(":8080", router)
}
