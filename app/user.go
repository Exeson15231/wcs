package app

import (
	"wcs/models"
)

func CreateUser(newUser models.User, env *Env) models.User {
	//TODO: validation

	var userId int64
	err := env.Db.QueryRow(
		`INSERT INTO users(
			username
		) VALUES (
			$1
		) RETURNING id`,
		newUser.Username,
	).Scan(&userId)
	if err != nil {
		// TODO: error rather than panic
		panic(err)
	}

	stmt, err := env.Db.Prepare(
		`INSERT INTO email(
			fk_users_id,
			address,
			subscribed
		) VALUES (
			$1, $2, $3
		)`,
	)
	if err != nil {
		// TODO: error rather than panic
		panic(err)
	}
	_, err = stmt.Exec(
		userId,
		newUser.Email.Address,
		false,
	)
	if err != nil {
		// TODO: error rather than panic
		panic(err)
	}

	stmt, err = env.Db.Prepare(
		`INSERT INTO security(
			fk_users_id,
			plain_text_password
		) VALUES (
			$1, $2
		)`,
	)
	if err != nil {
		// TODO: error rather than panic
		panic(err)
	}
	_, err = stmt.Exec(
		userId,
		newUser.Security.PlainTextPassword,
	)
	if err != nil {
		// TODO: error rather than panic
		panic(err)
	}

	return GetUserById(userId, env)
}

func UpdateUser(newDetails models.User, env *Env) models.User {
	userId := GetUserByUsername(newDetails.Username, env).Id

	stmt, err := env.Db.Prepare(
		`UPDATE 
			email
		SET (
			address 
		) = (
			$1
		) WHERE (
			fk_users_id = $2
		)`,
	)
	if err != nil {
		// TODO: error rather than panic
		panic(err)
	}
	_, err = stmt.Exec(
		newDetails.Email.Address,
		userId,
	)
	if err != nil {
		// TODO: error rather than panic
		panic(err)
	}

	return GetUserById(userId, env)
}

func GetAllUsers(env *Env) []models.User {
	users := []models.User{}
	rows, err := env.Db.Query(
		`SELECT
			users.id,
			users.business_id,
			users.username,
			security.id,
			security.plain_text_password, 
			email.id,
			email.address,
			email.subscribed
		FROM
			users
			JOIN security ON (users.id = security.fk_users_id)
			JOIN email ON (users.id = email.fk_users_id)`,
	)
	defer rows.Close()

	if err != nil {
		// TODO: error rather than panic
		panic(err)
	}

	for rows.Next() {
		var u models.User
		err := rows.Scan(
			&u.Id,
			&u.BusinessId,
			&u.Username,
			&u.Security.Id,
			&u.Security.PlainTextPassword,
			&u.Email.Id,
			&u.Email.Address,
			&u.Email.Subscribed,
		)
		if err != nil {
			// TODO: error rather than panic
			panic(err)
		}

		users = append(users, u)
	}
	err = rows.Err()
	if err != nil {
		// TODO: error rather than panic
		panic(err)
	}

	return users
}

func GetUserById(id int64, env *Env) models.User {
	var u models.User
	err := env.Db.QueryRow(
		`SELECT
			users.id,
			users.business_id,
			users.username,
			security.id,
			security.plain_text_password, 
			email.id,
			email.address,
			email.subscribed
		FROM
			users
			JOIN security ON (users.id = security.fk_users_id)
			JOIN email ON (users.id = email.fk_users_id)
		WHERE
			users.id = $1`,
		id,
	).Scan(
		&u.Id,
		&u.BusinessId,
		&u.Username,
		&u.Security.Id,
		&u.Security.PlainTextPassword,
		&u.Email.Id,
		&u.Email.Address,
		&u.Email.Subscribed,
	)
	if err != nil {
		// TODO: error rather than panic
		panic(err)
	}

	return u
}

func GetUserByUsername(username string, env *Env) models.User {
	var u models.User
	err := env.Db.QueryRow(
		`SELECT
			users.id,
			users.business_id,
			users.username,
			security.id,
			security.plain_text_password, 
			email.id,
			email.address,
			email.subscribed
		FROM
			users
			JOIN security ON (users.id = security.fk_users_id)
			JOIN email ON (users.id = email.fk_users_id)
		WHERE
			users.username = $1`,
		username,
	).Scan(
		&u.Id,
		&u.BusinessId,
		&u.Username,
		&u.Security.Id,
		&u.Security.PlainTextPassword,
		&u.Email.Id,
		&u.Email.Address,
		&u.Email.Subscribed,
	)
	if err != nil {
		// TODO: error rather than panic
		panic(err)
	}

	return u
}
