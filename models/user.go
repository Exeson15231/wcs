package models

type User struct {
	Id         int64
	BusinessId int64
	Username   string
	Security   Security
	Email      Email
}
