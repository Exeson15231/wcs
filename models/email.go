package models

type Email struct {
	Id         int64
	UserId     int64
	Address    string
	Subscribed bool
}
