package models

import (
	"database/sql"
	"fmt"
	"github.com/gchaincl/dotsql"
	_ "github.com/lib/pq"
)

func Connect(host, port, user, dbname, password string) (*sql.DB, error) {
	openCmd := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s",
		host,
		port,
		user,
		dbname,
		password)
	return sql.Open("postgres", openCmd)
}

func CreateTables(pathToScript string,
	db *sql.DB,
	dropExecs []string,
	createExecs []string) {

	dot, err := dotsql.LoadFromFile(pathToScript)
	if err != nil {
		// TODO: handle err rather than panic
		panic(err)
	}

	for _, cmd := range dropExecs {
		_, err := dot.Exec(db, cmd)
		if err != nil {
			// TODO: handle err rather than panic
			panic(err)
		}
	}

	for _, cmd := range createExecs {
		_, err := dot.Exec(db, cmd)
		if err != nil {
			// TODO: handle err rather than panic
			panic(err)
		}
	}
}
