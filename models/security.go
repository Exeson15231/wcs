package models

type Security struct {
	Id                int64
	UserId            int64
	PlainTextPassword string
}
