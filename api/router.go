package api

import (
	"github.com/gorilla/mux"
	"net/http"
	"wcs/app"
)

type HandlerFuncWithEnv func(*app.Env) http.Handler

type Route struct {
	Name        string
	Method      string
	Path        string
	HandlerFunc HandlerFuncWithEnv
}

type RouteSection []Route
type Routes []RouteSection

var routes = Routes{
	indexRoutes,
	userRoutes,
}

func GenerateRouter(env *app.Env) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, routeSection := range routes {
		for _, route := range routeSection {
			router.Methods(route.Method).
				Path(route.Path).
				Name(route.Name).
				Handler(route.HandlerFunc(env))
		}
	}
	return router
}
