package api

import (
	"fmt"
	"net/http"
	"wcs/app"
)

func Index(env *app.Env) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "Index page")
	})
}
