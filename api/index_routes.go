package api

var indexRoutes = RouteSection{
	Route{
		Name:        "Index",
		Method:      "GET",
		Path:        "/",
		HandlerFunc: Index,
	},
}
