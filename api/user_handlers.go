package api

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"net/http"
	"wcs/app"
)

func GetUsers(env *app.Env) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		users := app.GetAllUsers(env)
		returnedUsers := []UserDto{}
		for _, user := range users {
			returnedUsers = append(returnedUsers, ToDto(user))
		}

		// TODO: make util function
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(w).Encode(returnedUsers); err != nil {
			// TODO: turn panic into error
			panic(err)
		}
	})
}

func GetUser(env *app.Env) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		urlVars := mux.Vars(r)
		username := urlVars["username"]
		user := app.GetUserByUsername(username, env)
		// TODO: make util function
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(w).Encode(user); err != nil {
			// TODO: turn panic into error
			panic(err)
		}
	})
}

func CreateUser(env *app.Env) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var inputUser UserPostDto
		body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
		if err != nil {
			// TODO: remove panic
			panic(err)
		}
		if err := r.Body.Close(); err != nil {
			panic(err)
		}
		if err := json.Unmarshal(body, &inputUser); err != nil {
			// TODO: move general errors to utils
			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(422) // unprocessable entity
			if err := json.NewEncoder(w).Encode(err); err != nil {
				panic(err)
			}
		}

		errors := inputUser.Validate()
		if errors != nil {
			// TODO: add error handling
			panic(err)
		}

		newUser := inputUser.ToModel()
		newUser = app.CreateUser(newUser, env)
		returnUser := ToDto(newUser)

		// TODO: utility response functions
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusCreated)
		if err := json.NewEncoder(w).Encode(returnUser); err != nil {
			// TODO: replace panic with error
			panic(err)
		}
	})
}

func UpdateUser(env *app.Env) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		urlVars := mux.Vars(r)
		username := urlVars["username"]

		body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
		if err != nil {
			// TODO: remove panic
			panic(err)
		}
		if err := r.Body.Close(); err != nil {
			panic(err)
		}

		var inputUser UserPutDto
		if err := json.Unmarshal(body, &inputUser); err != nil {
			// TODO: handle error properly
			panic(err)
		}

		errors := inputUser.Validate()
		if errors != nil {
			// TODO: usual message about handling errors
			panic(err)
		}

		updateUser := inputUser.ToModel(username)
		updatedUser := app.UpdateUser(updateUser, env)
		returnUser := ToDto(updatedUser)

		// TODO: utility response functions
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusCreated)
		if err := json.NewEncoder(w).Encode(returnUser); err != nil {
			// TODO: replace panic with error
			panic(err)
		}
	})
}
