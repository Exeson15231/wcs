package api

import (
	"fmt"
)

type ValidationError struct {
	Key    string
	Value  string
	Reason string
}

func (e ValidationError) Error() string {
	return fmt.Sprintf("Value '%s' for key '%s' invalid. Reason: %s", e.Value, e.Key, e.Reason)
}

func ValidateNotEmptyString(key string, value string) (bool, ValidationError) {
	if value == "" {
		err := ValidationError{
			Key:    key,
			Value:  value,
			Reason: "Cannot be empty",
		}
		return true, err
	}
	return false, ValidationError{}
}
