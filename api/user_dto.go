package api

import (
	//"encoding/json"
	"wcs/models"
)

type UserDto struct {
	Id           int64  `json:"id"`
	Username     string `json:"username"`
	EmailAddress string `json:"emailAddress"`
}

func ToDto(u models.User) UserDto {
	return UserDto{
		Id:           u.BusinessId,
		Username:     u.Username,
		EmailAddress: u.Email.Address,
	}
}

type UserPostDto struct {
	Username     string `json:"username"`
	EmailAddress string `json:"emailAddress"`
	Password     string `json:"password"`
}

func (u *UserPostDto) ToModel() models.User {
	sec := models.Security{
		PlainTextPassword: u.Password,
	}

	email := models.Email{
		Address:    u.EmailAddress,
		Subscribed: false,
	}

	user := models.User{
		Username: u.Username,
		Security: sec,
		Email:    email,
	}

	return user
}

func (u UserPostDto) Validate() []ValidationError {
	var errors []ValidationError
	if failed, err := ValidateNotEmptyString("Username", u.Username); failed {
		errors = append(errors, err)
	}

	if failed, err := ValidateNotEmptyString("Password", u.Password); failed {
		errors = append(errors, err)
	}

	if failed, err := ValidateNotEmptyString("EmailAddress", u.EmailAddress); failed {
		errors = append(errors, err)
	}

	if len(errors) > 0 {
		return errors
	}
	return nil
}

type UserPutDto struct {
	EmailAddress string `json:"emailAddress"`
}

func (u UserPutDto) ToModel(username string) models.User {
	email := models.Email{
		Address: u.EmailAddress,
	}

	user := models.User{
		Username: username,
		Security: models.Security{},
		Email:    email,
	}

	return user
}

func (u UserPutDto) Validate() []ValidationError {
	var errors []ValidationError
	if failed, err := ValidateNotEmptyString("EmailAddress", u.EmailAddress); failed {
		errors = append(errors, err)
	}

	if len(errors) > 0 {
		return errors
	}
	return nil
}
