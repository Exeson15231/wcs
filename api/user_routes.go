package api

var userRoutes = RouteSection{
	Route{
		Name:        "GetUsers",
		Method:      "GET",
		Path:        "/users",
		HandlerFunc: GetUsers,
	},
	Route{
		Name:        "GetUser",
		Method:      "GET",
		Path:        "/users/{username}",
		HandlerFunc: GetUser,
	},
	Route{
		Name:        "CreateUser",
		Method:      "POST",
		Path:        "/users",
		HandlerFunc: CreateUser,
	},
	Route{
		Name:        "UpdateUser",
		Method:      "PUT",
		Path:        "/users/{username}",
		HandlerFunc: UpdateUser,
	},
}
